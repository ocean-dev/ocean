# Ocean

This repo contains the Docker Compose files to use when running Ocean
locally, for development and evaluation. It's practical to run Ocean from
this directory when running the entire system. For work on individual services,
see their respective repos.


## Directory structure

Create a directory for Ocean somewhere. Its name isn't important; "Ocean" works
well. Install all ocean repos, including this one, in that folder:
```
  Ocean
    |
    +-- admin-client
    +-- auth
    +-- cloud
    +-- cms
    +-- jobs
    +-- mail
    +-- ocean
    +-- rollout
    +-- varnish
```
The paths used for Docker Compose assume the above arrangement. There is a shell
script to set it up. Navigate to the directory where you want the `Ocean`
directory and its contents to be created. Then execute the following:
```
  ./download_repos.sh
```
The `Ocean` directory will be created, and the repos downloaded and installed
as per the description above.


## Local persistent storage

The directories `mysql_data` and `localstack_data` in this repo are bound by
docker-compose as Docker volumes and are used for persistent storage. Their
contents are excluded from source control.


## Starting Ocean

To start Ocean locally, simply `cd` to this directory. First build the local
infrastructure needed by the services – the database, the Varnish cache, and
the AWS mock services:
```
  docker-compose -f common.yml build
```
Then build the services:
```
  docker-compose -f services.yml build
```
You can do the above in parallel if you want, using two separate terminal
windows. When all has been built, bring up the common infrastructure:
```
  docker-compose -f common.yml up -d
```
This starts the database and the AWS mock services in daemonised
mode. You can of course omit the `-d` and use additional terminal tabs or
windows for the subsequent commands.

The first time you run Ocean locally, you must also initialise its databases.
This includes creating and migrating the SQL DBs, but it also creates all
DynamoDB tables and associated data. Use the following command:
```
  ./setup_all.sh
```
This command simply executes the following:
```
  docker-compose -f services.yml run --rm auth_service rake ocean:setup_all
  docker-compose -f services.yml run --rm cloud_service rake ocean:setup_all
  docker-compose -f services.yml run --rm cms_service rake ocean:setup_all
  docker-compose -f services.yml run --rm jobs_service rake ocean:setup_all
  docker-compose -f services.yml run --rm mail_service rake ocean:setup_all
  docker-compose -f services.yml run --rm rollout_service rake ocean:setup_all
```
On AWS, the above commands will be run automatically for you as part of the
installation procedure, but when running locally you must execute them yourself.
Cf the repo for each service for more information on what each command does and
when you might to want to run them.

Finally, to bring up all the Ocean services:
```
  docker-compose -f services.yml up -d
```
Note: You can of course omit the `-d` switch to see the aggregated logs; if you
do so, you will have to open another terminal window to continue.

# Shutdown
Execute the following:
```
  docker-compose -f services.yml down
  docker-compose -f common.yml down
```
If you quit using Control-C, you may want to issue the same commands in order to
close down any remaining containers.
