#!/bin/sh

WD=$PWD/..

cd $WD/admin-client && git pull
cd $WD/auth && git pull
cd $WD/cfn-templates && git pull
cd $WD/cloud && git pull
cd $WD/cms && git pull
cd $WD/jobs && git pull
cd $WD/mail && git pull
cd $WD/ocean && git pull
cd $WD/rollout && git pull
cd $WD/sandbox && git pull
cd $WD/varnish && git pull

cd $WD/ocean
