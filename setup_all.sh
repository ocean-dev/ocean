#!/bin/sh

docker-compose -f services.yml run --rm auth_service rake ocean:setup_all
docker-compose -f services.yml run --rm cloud_service rake ocean:setup_all
docker-compose -f services.yml run --rm cms_service rake ocean:setup_all
docker-compose -f services.yml run --rm jobs_service rake ocean:setup_all
docker-compose -f services.yml run --rm mail_service rake ocean:setup_all
docker-compose -f services.yml run --rm rollout_service rake ocean:setup_all
