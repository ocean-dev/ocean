#!/bin/sh

mkdir Ocean
cd Ocean

git clone git@gitlab.com:ocean-dev/admin_client.git
git clone git@gitlab.com:ocean-dev/auth.git
git clone git@gitlab.com:ocean-dev/cfn-templates.git
git clone git@gitlab.com:ocean-dev/cloud.git
git clone git@gitlab.com:ocean-dev/cms.git
git clone git@gitlab.com:ocean-dev/jobs.git
git clone git@gitlab.com:ocean-dev/mail.git
git clone git@gitlab.com:ocean-dev/ocean.git
git clone git@gitlab.com:ocean-dev/rollout.git
git clone git@gitlab.com:ocean-dev/sandbox.git
git clone git@gitlab.com:ocean-dev/varnish.git

cd ..
