#!/bin/sh

docker-compose -f services.yml build admin_client
docker tag ocean_admin_client peterbengtson/ocean_admin_client:dev
docker push peterbengtson/ocean_admin_client:dev

docker tag ocean_admin_client peterbengtson/ocean_admin_client:latest
docker push peterbengtson/ocean_admin_client:latest


docker-compose -f services.yml build auth_service
docker tag ocean_auth_service peterbengtson/ocean_auth_service:dev
docker push peterbengtson/ocean_auth_service:dev

docker tag ocean_auth_service peterbengtson/ocean_auth_service:latest
docker push peterbengtson/ocean_auth_service:latest


docker-compose -f services.yml build cloud_service
docker tag ocean_cloud_service peterbengtson/ocean_cloud_service:dev
docker push peterbengtson/ocean_cloud_service:dev

docker tag ocean_cloud_service peterbengtson/ocean_cloud_service:latest
docker push peterbengtson/ocean_cloud_service:latest


docker-compose -f services.yml build cms_service
docker tag ocean_cms_service peterbengtson/ocean_cms_service:dev
docker push peterbengtson/ocean_cms_service:dev

docker tag ocean_cms_service peterbengtson/ocean_cms_service:latest
docker push peterbengtson/ocean_cms_service:latest


docker-compose -f services.yml build jobs_service
docker tag ocean_jobs_service peterbengtson/ocean_jobs_service:dev
docker push peterbengtson/ocean_jobs_service:dev

docker tag ocean_jobs_service peterbengtson/ocean_jobs_service:latest
docker push peterbengtson/ocean_jobs_service:latest


docker-compose -f services.yml build mail_service
docker tag ocean_mail_service peterbengtson/ocean_mail_service:dev
docker push peterbengtson/ocean_mail_service:dev

docker tag ocean_mail_service peterbengtson/ocean_mail_service:latest
docker push peterbengtson/ocean_mail_service:latest


docker-compose -f services.yml build rollout_service
docker tag ocean_rollout_service peterbengtson/ocean_rollout_service:dev
docker push peterbengtson/ocean_rollout_service:dev

docker tag ocean_rollout_service peterbengtson/ocean_rollout_service:latest
docker push peterbengtson/ocean_rollout_service:latest


docker-compose -f common.yml build varnish
docker tag ocean_varnish peterbengtson/ocean_varnish:dev
docker push peterbengtson/ocean_varnish:dev

docker tag ocean_varnish peterbengtson/ocean_varnish:latest
docker push peterbengtson/ocean_varnish:latest
